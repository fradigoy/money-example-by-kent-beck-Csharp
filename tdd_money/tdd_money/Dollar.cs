﻿using System;

namespace tdd_money
{
    class Dollar
    {
        private int amount;

        public Dollar(int amount)
        {
            this.amount = amount;

        }

        public int times(int multiplier)
        {
            amount = amount * multiplier;

            return amount;
        }

        public bool equals(Dollar dollar)
        {
           if( this.amount == dollar.amount)
                return true;
            return false;

        }
    }
}